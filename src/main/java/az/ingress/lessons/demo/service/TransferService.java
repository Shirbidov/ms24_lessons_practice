package az.ingress.lessons.demo.service;


public interface TransferService {
    void transfer(Long sourceAccountID, Long targetAccountID, Double amount );
}
