package az.ingress.lessons.demo.service;

import az.ingress.lessons.demo.dto.StudentDto;
import az.ingress.lessons.demo.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class  StudentServiceImp implements StudentService {

    private final ModelMapper modelMapper;
    private final StudentRepository studentRepository;


    @Override
    public List<StudentDto> listStudent() {
        return studentRepository.findAllJpql()
                .stream()
                .map(studentEntity -> modelMapper.map(studentEntity , StudentDto.class))
                .toList();
    }
}
