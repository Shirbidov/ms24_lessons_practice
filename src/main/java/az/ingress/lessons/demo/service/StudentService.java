package az.ingress.lessons.demo.service;



import az.ingress.lessons.demo.dto.StudentDto;

import java.util.List;

public interface StudentService {

    List<StudentDto> listStudent();


}
