package az.ingress.lessons.demo.repository;

import az.ingress.lessons.demo.domain.StudentEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity,Long> {
    @EntityGraph(attributePaths = "courses")
    List<StudentEntity> findAll();

//    List<StudentEntity> findAllNativeQuery();

    @Query("From StudentEntity s JOIN FETCH s.courses c")
    List<StudentEntity> findAllJpql();



}
