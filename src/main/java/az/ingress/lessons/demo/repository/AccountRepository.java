package az.ingress.lessons.demo.repository;

import az.ingress.lessons.demo.dto.AccountEntity;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<AccountEntity, Long> {


    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<AccountEntity> findById(Long id);

}
